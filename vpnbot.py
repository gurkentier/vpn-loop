import pyautogui
import time


class VpnSwitcher:
    def __init__(self,
                 pag=pyautogui,
                 PP_X=1000,
                 PP_DROPDOWN_OFFSET=220,
                 PP_ICO_X=990,
                 PP_ICO_Y=0,
                 PP_BTN_X=1000,
                 PP_BTN_Y=79,
                 PP_FAV_ROOT=101,
                 CURRENT_FAV_POS=101,
                 FAV_OFFSET_SIZE=20,
                 FAV_COUNT=24
                 ):
        self.pag = pag
        self.PP_X = PP_X
        self.PP_DROPDOWN_OFFSET = PP_DROPDOWN_OFFSET
        self.PP_ICO_X = PP_ICO_X
        self.PP_ICO_Y = PP_ICO_Y
        self.PP_BTN_X = PP_BTN_X
        self.PP_BTN_Y = PP_BTN_Y
        self.PP_FAV_ROOT = PP_FAV_ROOT
        self.CURRENT_FAV_POS = CURRENT_FAV_POS
        self.FAV_OFFSET_SIZE = FAV_OFFSET_SIZE
        self.FAV_COUNT = FAV_COUNT

    def connectNextFav(self, Y):
        self.pag.moveTo(self.PP_X + self.PP_DROPDOWN_OFFSET, self.CURRENT_FAV_POS)
        self.CURRENT_FAV_POS = \
            self.PP_FAV_ROOT \
            if (self.CURRENT_FAV_POS >= self.PP_FAV_ROOT + (self.FAV_OFFSET_SIZE * self.FAV_COUNT))\
            else self.CURRENT_FAV_POS + self.FAV_OFFSET_SIZE
        self.pag.click()

    def openDropdown(self):
        self.pag.moveTo(self.PP_ICO_X, self.PP_ICO_Y)
        time.sleep(1)
        self.pag.moveTo(self.PP_ICO_X, self.PP_ICO_Y + 5)
        self.pag.click()

    def chooseBtn(self):
        self.pag.moveTo(self.PP_BTN_X, self.PP_BTN_Y)
        self.pag.click()

    def keyPress(self, key):
        self.pag.keyDown(key)
        self.pag.keyUp(key)

    def openFavs(self):
        self.pag.moveTo(self.PP_X, self.PP_FAV_ROOT)
        time.sleep(0.3)
        self.pag.moveTo(self.PP_X + self.PP_DROPDOWN_OFFSET, self.PP_FAV_ROOT)

    def findOffset(self):
        self.pag.displayMousePosition()

    def loop(self, cb, epochs=100, delayBeforeSwitch=3, delayAfterSwitch=5):
        for x in range(epochs):
            cb()
            time.sleep(delayBeforeSwitch)
            self.openDropdown()
            time.sleep(0.5)
            self.openFavs()
            time.sleep(0.5)
            self.connectNextFav(self.CURRENT_FAV_POS)
            time.sleep(delayAfterSwitch)
